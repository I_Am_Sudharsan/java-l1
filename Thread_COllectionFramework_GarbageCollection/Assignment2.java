package Thread_Collection_GarbageCollection;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Assignment2 extends Thread{

	public static void main(String[] args) {
		Timesay t=new Timesay();
		t.start();

	}

}

class Timesay extends Thread{
	public void run() {
		 SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");  
		 
		for(int i=0;i<10;i++) {
				Date date = new Date(); 
			    System.out.println(formatter.format(date)); 
			    
			    try {Thread.sleep(2000);} catch (InterruptedException e) {}
			    
		}
	}
}
