package Thread_Collection_GarbageCollection;

import java.util.HashSet;
import java.util.Scanner;
import java.util.*;


public class Assignment5 {

	public static void main(String[] args) {
		HashSet<String> data=new HashSet<String>();
		Scanner in=new Scanner(System.in);

		System.out.println("Enter number of employees :");
		int n=in.nextInt();
		
		for(int i=0;i<n;i++) {
			System.out.println("Enter Name of employee :");
			String name=in.next();
			data.add(name);
		}
		
		Iterator it = data.iterator();
		
		System.out.println("\n\nThe employee values are: "); 
        while (it.hasNext()) { 
            System.out.println(it.next()); 
        } 
	}

}
