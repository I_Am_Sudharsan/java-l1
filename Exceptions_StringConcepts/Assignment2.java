package Exception_String_Concepts;



import java.util.Scanner;

public class Assignment2 {

	public static void main(String[] args) {
		Students stu=new Students();
		int[] m=stu.getdata();
		stu.calc(m);
	}

}

class Students{
	String name="";
	Scanner in =new Scanner(System.in);

	
	public int[] getdata() {
		int[] m=new int[3];

		try {
			System.out.println("Enter name of the student :");
			name=in.next();
			System.out.println("Mark 1 :");
			m[0]=in.nextInt();
			System.out.println("Mark 2 :");
			m[1]=in.nextInt();
			System.out.println("Mark 3 :");
			m[2]=in.nextInt();
			
			
		} catch (Exception e) {
			System.out.println("Mark cannot accept alphabets...!!!!");
		}
		
		return m;

	}
	
	public void calc(int[] m) {
		int total=m[0]+m[1]+m[2];
		System.out.println("Total acquired :"+total);
	}
	
	
}
