package Exception_String_Concepts;

public class Assignment1 {

	public static void main(String[] args) {
		String name="";
		int age=0;
		try {
			name=args[0];
			age=Integer.parseInt(args[1]);
			if((age<18)||(age>60)) {
				throw new Exception("Invalid age..!!");
			}else {
				System.out.println("Name :"+name+"\nAge :"+age);
			}
		}catch(Exception e) {
			System.out.println("Sorry....!! Exception Occurs...!!");
			System.out.println(e);
		}

	}

}
