package Object_Oriented_Concepts;

import java.util.Scanner;

public class Assignment3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in=new Scanner(System.in);
		System.out.println("Do you want to add : \n\t1.Novel\n\t2.Magazine\nEnter :");
		int opt=in.nextInt();
		if(opt==1) {
			Novel nov=new Novel();
			nov.gettype();
			nov.disp();
		}else if(opt==2) {
			Magazine mag=new Magazine();
			mag.gettype();
			mag.disp();
		}else {
			System.out.println("Wroung input");
		}
		
		
	}

}

class Book{
	int ISBN=0;
	String name="";
	int price=0;
	Scanner in=new Scanner(System.in);
	
	public void getdata() {
		System.out.println("Enter ISBN Number :");
		ISBN=in.nextInt();
		System.out.println("\nEnter name of the book :");
		name=in.next();
		System.out.println("\nEnter price of the book :");
		price=in.nextInt();
	}
}

class Novel extends Book{
	Novel(){
		getdata();
	}
	
	String author="";
	public void gettype() {
		System.out.println("Enter author name :");
		author=in.next();
	}
	
	public void disp() {
		System.out.println("\n\nBook details are :");
		System.out.println("ISBN :"+ISBN+" Name :"+name+" Author :"+author+" Price :"+price);
	}
}

class Magazine extends Book{
	public Magazine() {
		getdata();
	}
	String type="";
	public void gettype() {
		System.out.println("Enter Magazine type :");
		type=in.next();
	}
	
	public void disp() {
		System.out.println("\n\nBook details are :");
		System.out.println("ISBN :"+ISBN+"\nName :"+name+"\nType :"+type+"\nPrice :"+price);
	}
}
