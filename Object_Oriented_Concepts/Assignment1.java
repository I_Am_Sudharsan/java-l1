package Object_Oriented_Concepts;

import java.util.Scanner;

public class Assignment1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String[][] library= {{"001","Kitchen Knife","Damu","235"},{"002","Home Remedy","Siddha Guru","300"},{"003","Fundamental Java","James Goslings","1500"},{"004","Harry Potter","J K Rowling","1900"}};
		
		Operations op=new Operations();
		op.disp(library);
		String opt=op.getdata();
		int[] val=op.getdis(opt, library);
		op.result(val);
		
		
	}

}


class Operations{
	
	Scanner in=new Scanner(System.in);
	
	Operations() {
		System.out.println("\nWelcome to Library....!!!");
		System.out.println("\n\nHere are the available books :\n");
	}
	
	public void disp(String[][] lib) {
		System.out.println("ISBN   Name               Author    Price");
		for(int i=0;i<lib.length;i++) {
			System.out.println(lib[i][0]+"   "+lib[i][1]+"          "+lib[i][2]+"    "+lib[i][3]);
		}
	}
	
	public String getdata() {
		
		System.out.println("\nEnter ISBN of your book :");
		String opt=in.next();
		return opt;
	}
	
	public int[] getdis(String opt,String[][] lib) {
		String a=opt;
		String[][] b=lib;
		int[] val=new int[2];
		
		for(int i=0;i<lib.length;i++) {
			if(b[i][0].contentEquals(a)) {
				System.out.println("\nSelected Book :");
				System.out.println(b[i][0]+"  "+b[i][1]+"  "+b[i][2]+"  "+b[i][3]);
				System.out.println("\nEnter discount percentage :");
				val[0]=in.nextInt();
				val[1]=Integer.parseInt(b[i][3]);
				break;
			}
			
		}
		if(val[1]==0) {
			System.out.println("Wroung value selected...!");
		}
		return val;
	}
	public void result(int val[]) {
		if(val[1]!=0) {
			int off=val[0]-100;
			int total=(val[1]/100)*off;
			if(total<0) {
				total*=-1;
			}
			System.out.println("Total Amount is :"+total);
		}
		
	}
}
