package Object_Oriented_Concepts;

import java.util.Scanner;

public class Assignment5 {

	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		int[] ins=new int[10];
		System.out.println("Enter ten series of options from given :");
		System.out.println("\t1.Piano\n\t2.Flute\n\t3.Guitar\nEnter numbers:");
		
		for(int i=0;i<10;i++) {
			ins[i]=in.nextInt();
		}
		
		Piano p=new Piano();
		Flute f=new Flute();
		Guitar g=new Guitar();
		for(int i=0;i<10;i++) {
			switch (ins[i]) {
			case 1:
				p.play();
				break;
			case 2:
				f.play();
				break;
			case 3:
				g.play();
				break;
			default:
				break;
			}
		}
		
		System.out.println("\nChecking the instance :");
		 System.out.println(p instanceof Instrument);
		 System.out.println(f instanceof Instrument);
		 System.out.println(g instanceof Instrument);
		
	}

}

abstract class Instrument{
	public abstract void play();
}

class Piano extends Instrument{

	@Override
	public void play() {
		System.out.println("Piano is playoing tan tan tan tan");
		
	}
	
}

class Flute extends Instrument{

	@Override
	public void play() {
		System.out.println("Flute is playing toot toot toot toot");
		
	}
	
}

class Guitar extends Instrument{

	@Override
	public void play() {
		System.out.println("Guitar is palying tin tin tin");
		
	}
	
}