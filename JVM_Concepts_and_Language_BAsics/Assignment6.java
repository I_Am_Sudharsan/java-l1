package jvm_concepts;



import java.util.Scanner;


public class Assignment6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int maxval=0;
		Scanner in=new Scanner(System.in);
		System.out.println("Enter number of digits :");
		int n=in.nextInt();
		
		int[] val=new int[n];
		
		System.out.println("Enter the value:");
		for(int i=0;i<n;i++) {
			val[i]=in.nextInt();
		}
		
		for(int i=0;i<n;i++) {
			if(val[i]>maxval) {
				maxval=val[i];
			}
		}
		System.out.println("The maximum value in the array is :"+maxval);
		
	}

}
