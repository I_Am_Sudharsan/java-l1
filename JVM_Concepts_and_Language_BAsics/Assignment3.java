package jvm_concepts;

import java.util.Scanner;

public class Assignment3 {

	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		
		System.out.println("Enter number of minutes :");
		int min=in.nextInt();
		
		int hours=min/60;
		int remmins=min%60;
		
		if(hours>24) {
			int days=hours/24;
			if(days>364) {
				int year=days/364;
				int day=days%364;
				System.out.println("\nThe minutes consits of "+year+" years and "+day+" days");
			}else {
				System.out.println("\nThe minutes consits of "+days+" days");
			}
			
		}

	}

}
