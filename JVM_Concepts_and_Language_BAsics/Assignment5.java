package jvm_concepts;

import java.util.Scanner;

public class Assignment5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Enter a four digit number:");
		Scanner in=new Scanner(System.in);
		int n=in.nextInt();
		
		if((n>999)&&(n<10000)){
			//getting first digit
			int v=n/1000;
			int res=0;
			res+=v;
			n=n%1000;

			
			//getting second digit
			v=n/100;
			res+=v;
			n=n%100;
			
			//getting third digit
			v=n/10;
			res+=v;
			n=n%10;
	
			//getting fourth digit
			res+=n;
			
			System.out.println("The sum of four digits are :"+res);
		}else {
			System.out.println("Enter a four digit number...!");
		}

	}

}
