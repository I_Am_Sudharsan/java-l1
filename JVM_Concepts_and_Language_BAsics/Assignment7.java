package jvm_concepts;

import java.util.Scanner;

public class Assignment7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		System.out.println("Enter a number to find its factorial :");
		
		Scanner in=new Scanner(System.in);
		int n=in.nextInt();
		int fact=1;
		
		for(int i=n;i>0;i--) {
			fact=fact*i;
		}
		System.out.println("\nThe factorial of the number is:"+fact);
	}

}
